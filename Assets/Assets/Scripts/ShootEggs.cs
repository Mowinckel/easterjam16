﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ShootEggs : MonoBehaviour {

    public GameObject eggPrefab;
	public AudioClip[] throwingSounds;
	private SoundSpawner soundManager;
    public bool canShoot = true;
    public bool canShootPub = true;
    public float coolDowntime = 5.0f;
    public static ShootEggs singleton;
    public Image coolDownTopGUI;

    public void Awake()
    {
        if(singleton == null)
        {
            singleton = this;
        }
        else
        {
            Destroy(this);
        }
    }


	// Use this for initialization
	void Start () {
		soundManager = GetComponent<SoundSpawner> ();
        coolDownTopGUI.fillAmount = 1.0f;
    }
	
	// Update is called once per frame
	void Update () {


        if (!canShoot && coolDownTopGUI.fillAmount < (1f - 2 * (Time.deltaTime / ShootEggs.singleton.coolDowntime)))
        {
            coolDownTopGUI.fillAmount += Time.deltaTime / ShootEggs.singleton.coolDowntime;
        }
        else if (!canShoot && coolDownTopGUI.fillAmount == 1f)
        {

            coolDownTopGUI.fillAmount = 0;


        }
        else if (!canShoot && coolDownTopGUI.fillAmount + (Time.deltaTime / ShootEggs.singleton.coolDowntime) > 0.999f  || canShoot)
        {
            coolDownTopGUI.fillAmount = 1;
        }


        if (MenuScript.joystick) {
			if (Input.GetAxis("PrimaryAttack") != 0) {
				if (canShoot) {
					StartCoroutine (ThrowEgg());
					//Debug.Log ("Egg!");
				}
					
			}
		}
		else {
			//Cursor.visible = false;
			if (Input.GetMouseButtonDown(0)) {
				if (canShoot) {
					StartCoroutine (ThrowEgg());
					//Debug.Log ("Egg!");
                    
				}
			}
		}

    }	
public IEnumerator ThrowEgg () {
		Instantiate (eggPrefab, transform.position, Quaternion.identity);
		soundManager.PlaySoundArray(throwingSounds,0.9f);
        canShoot = false;
        yield return new WaitForSeconds(coolDowntime);
        canShoot = true;
    }
    //public void CDGUIMethod() // Used in Subclass CDGUI which is used for the cool down gui !!!DO NOT REMOVE!!!
    //{
    //    if (MenuScript.joystick)
    //    {
    //        if (Input.GetAxis("PrimaryAttack") < -0.2)
    //            if (canShoot && canShootPub)
    //                StartCoroutine(CoolDownMethod());
    //    }
    //    else
    //    {
    //        if (Input.GetMouseButtonDown(0))
    //            if (canShoot && canShootPub)
    //                StartCoroutine(CoolDownMethod());
    //    }
    //}
    //public IEnumerator CoolDownMethod() // Paralele coroutine used in CDGUIMethod() !!!DO NOT REMOVE!!!
    //    {
    //        canShootPub = false;      
    //        yield return new WaitForSeconds(coolDowntime);
    //        canShootPub = true;
    //    }
}