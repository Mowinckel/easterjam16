﻿using UnityEngine;
using System.Collections;

public class LoserScreenScript : MonoBehaviour {
public Canvas loserScreenCanvas;
	
	void Start () {
        loserScreenCanvas.enabled = true;
    } // Use this for initialization

    
    void Update () {
        if (Input.GetButtonDown("CButton1"))
        {
            ReplayButtonMethod(0);
        }
        else if (Input.GetButtonDown("Fire2"))
        {
            ReplayButtonMethod(1);
        }

    } // Update is called once per frame

    public void ReplayButtonMethod(int FID)
    {
        if(FID == 0)
        {
            ChangeLevel("Main");
        }
        if(FID == 1)
        {
            ChangeLevel("Menu");
        }

    }
    public void ChangeLevel(string levelName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(levelName);
    }

}
