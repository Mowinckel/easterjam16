﻿using UnityEngine;
using System.Collections;

public class ChangeIcon : MonoBehaviour {

	public Sprite glowSprite;
	public Sprite originalSprite;

	private SpriteRenderer spriteOnObject;

	// Use this for initialization
	void Start () {
		spriteOnObject = gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player")
			//Debug.Log ("Touched my parent");
		if (other.tag == "Enemy") {			
			spriteOnObject.sprite = glowSprite;
		}
	}
	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "Enemy") {			
			spriteOnObject.sprite = originalSprite;
		}
		if (other.tag == "MainCamera") {
			Debug.Log ("Reticle left camera view");
		}
	}
}
