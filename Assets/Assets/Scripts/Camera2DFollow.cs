using System;
using UnityEngine;

public class Camera2DFollow : MonoBehaviour
{
    public Transform target;
    public float damping = 1;
    public float lookAheadFactor = 3;
    public float lookAheadReturnSpeed = 0.5f;
    public float lookAheadMoveThreshold = 0.1f;
	public float horizontalBuffer = 0.0f;

	public float shakeTimer;
	public float shakeAmount;

    private float m_OffsetZ;
    private Vector3 m_LastTargetPosition;
    private Vector3 m_CurrentVelocity;
    private Vector3 m_LookAheadPos;





    // Use this for initialization
    private void Start()
    {

        m_LastTargetPosition = target.position;
        m_OffsetZ = (transform.position - target.position).z;
        transform.parent = null;
    }

	void Update () {
		if (shakeTimer >= 0) {
			Vector2 shakePos = UnityEngine.Random.insideUnitCircle * shakeAmount;

			transform.position = new Vector3(transform.position.x + shakePos.x, transform.position.y + shakePos.y, transform.position.z);

			shakeTimer -= Time.deltaTime;
		}
	}



    // Update is called once per frame
    private void LateUpdate()
    {
        // only update lookahead pos if accelerating or changed direction
        float xMoveDelta = (target.position - m_LastTargetPosition).x;

        bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

        if (updateLookAheadTarget)
        {
            m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
        }
        else
        {
            m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
        }

        Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;
		//Debug.Log(aheadTargetPos.x);
		if (aheadTargetPos.x < -15.0f) {
			float newX = aheadTargetPos.x - horizontalBuffer;
			aheadTargetPos.x = Mathf.SmoothDamp(aheadTargetPos.x, newX, ref m_CurrentVelocity.x, damping*2f);
		}
		if (aheadTargetPos.x > 15.0f) {
			float newX = aheadTargetPos.x + horizontalBuffer*4;
			aheadTargetPos.x = Mathf.SmoothDamp(aheadTargetPos.x, newX, ref m_CurrentVelocity.x, damping*2f);
		}
        Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

        transform.position = newPos;

        m_LastTargetPosition = target.position;


	
    }

	public void ShakeCamera(float shakePower, float shakeDuration) {
		shakeAmount = shakePower;
		shakeTimer = shakeDuration;
	}
}

