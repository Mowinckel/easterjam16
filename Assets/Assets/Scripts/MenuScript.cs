﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {
	public Canvas mainMenu;
	public Canvas creditsMenu;
	public Canvas controllerMenu;
	public AudioClip doorOpen;
    private bool buttonClicked = false;
    private SoundSpawner ss;
	private float delay; 
	private bool started = false;
	public static bool joystick = true;
	private bool mouseClicked = false;
	void Start()
	{
		Time.timeScale = 1;
		ss = GetComponent<SoundSpawner>();
		delay = doorOpen.length;
		controllerMenu.enabled = false;
		creditsMenu.enabled = false;
		mainMenu.enabled = true;
		CheckForInput();
//		mainMenu = GetComponent<Canvas>();
//		creditsMenu = GetComponent<Canvas>();
//		controllerMenu = GetComponent<Canvas>();
	}
	void Update()
	{
		// Creates controller input  reactions: sets values for the same scene canvas switch and the play button loadlevl, prevents button ghostingeb
		if (Input.GetButtonDown ("CButton1")) {
            if (!buttonClicked)
            {
                StartGame();
                buttonClicked = true;
            }

        } else if (Input.GetButtonDown ("Fire2")) {
			if(mainMenu.enabled == false)
			ChangeCanvas (0);
		} else if (Input.GetButtonDown ("Fire3")) {
			if(mainMenu.enabled == true)
			ChangeCanvas (1);
		} else if (Input.GetButtonDown ("Jump")) {
			if(mainMenu.enabled == true)
			ChangeCanvas (2);		
		}
        if (started)
        {
			
            if (buttonClicked) // Plays sound and enters level 
            {
				ss.PlaySingleSoundOnce(doorOpen, 1f);
                buttonClicked = false;
            }
			if (mouseClicked) {
				ss.PlaySingleSoundOnce(doorOpen, 1f);
				mouseClicked = false;
			}
			StartCoroutine("WaitForDoor");

        }
    }
	public void ChangeScene (string sceneName) //creates a loadlevel funtion
	{
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
    }

	public void ChangeCanvas (int canvasID)
	{
		switch (canvasID) { // Changes the canvasies visibility in the Menu-Scene
		case 0:
			{
				mainMenu.enabled = true;
				creditsMenu.enabled = false;
				controllerMenu.enabled = false;
				break;
			}
		case 1:
			{
				mainMenu.enabled = false;
				controllerMenu.enabled = true;
				creditsMenu.enabled = false;
				break;
			
			}
		case 2:
			{
				mainMenu.enabled = false;
				controllerMenu.enabled = false;
				creditsMenu.enabled = true;
				break;
			}
		default:
			mainMenu.enabled = true;
			controllerMenu.enabled = false;
			creditsMenu.enabled = true;
			break;

		}
	}
	void CheckForInput () { // checks for user input type
		Debug.Log(Input.GetJoystickNames().Length);
		if (Input.GetJoystickNames().Length != 0)
			MenuScript.joystick = true;
		else
			MenuScript.joystick = false;

		if (MenuScript.joystick)
			Debug.Log("Joystick is present");
		
		else
			Debug.Log("No joystick");
	}

	public void StartGame () // Used when changing scenes
	{
		Debug.Log("Game starting");
		started = true;
	}

	public void MouseClick () {
		mouseClicked = true;
	}

	IEnumerator WaitForDoor () {
		yield return new WaitForSeconds(delay);
		ChangeScene("Main");
	}
		
}