﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed = 4.0f;
	public float aimSpeed = 6.0f;
	public GameObject reticlePrefab;

	public AudioClip[] walking;
	public AudioClip stunSound;
	public Camera2DFollow camFollow;

	private SoundSpawner soundManager;

	private Rigidbody2D playerRB;
	private float deadPoint = 1f;
	private Rigidbody2D reticleRB;
	private Animator animator;

	private bool flip = true;
	private bool isRight = false;
	private bool moving = false;
	private bool stunnedSound = false;


	private float cooldownCounter = 3f;
//	private bool coolingdown = false;
	private bool stunned = false;

	private float soundTimer = 0f;
	// Use this for initialization
	void Start () {
		playerRB = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		soundManager = GetComponent<SoundSpawner> ();
		Vector3 playPosOffset = new Vector3 (transform.position.x + 2.0f, transform.position.y + 2.0f, 0);
		GameObject reticle = Instantiate (reticlePrefab, playPosOffset, Quaternion.identity) as GameObject;
		reticleRB = reticle.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		soundTimer -= Time.deltaTime;
		if (soundTimer < 0) {
			soundTimer = 0;
		}
		ResizeCollider();
	}
	void FixedUpdate () {

		if (!stunned) {
			Move ();
		} else {
			Stun ();
			cooldownCounter -= Time.deltaTime;
			if (!stunnedSound)
			{
				soundManager.PlaySingleSoundOnce (stunSound,0.9f);
				stunnedSound = true;
			}
			if (cooldownCounter <= 0) {
				cooldownCounter = 3f;
				stunned = false;
				animator.SetBool ("isStun", false);
				stunnedSound = false;
			}
		}

		Aim ();
	}

	public void Aim () {
		if (MenuScript.joystick)  //IF JOYSTICK PRESENT
		{
			//Moving reticle
			Vector2 move = new Vector2 (Mathf.Lerp (0, Input.GetAxis ("JoystickRX") * aimSpeed, 0.8f), Mathf.Lerp (0, Input.GetAxis ("JoystickRY") * aimSpeed, 0.8f));
			reticleRB.velocity = move;

			//Constraining to camera view
			Vector2 pos = Camera.main.WorldToViewportPoint(reticleRB.transform.position);
			if (pos.x > 1.0f)
			{
				Vector3 playerAtCam = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, pos.y, 0f));
				reticleRB.transform.position = new Vector2(playerAtCam.x, reticleRB.transform.position.y);
			}
			else if (pos.x < 0.0f)
			{
				Vector3 playerAtCam = Camera.main.ScreenToWorldPoint(new Vector3(0f, pos.y, 0f));
				reticleRB.transform.position = new Vector2(playerAtCam.x, reticleRB.transform.position.y);
			}
			else if (pos.y > 1.0f)
			{
				Vector3 playerAtCam = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, Screen.height, 0f));
				reticleRB.transform.position = new Vector2(reticleRB.transform.position.x, playerAtCam.y);
			}
			else if (pos.y < 0.0f)
			{
				Vector3 playerAtCam = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, 0.0f, 0f));
				reticleRB.transform.position = new Vector2(reticleRB.transform.position.x, playerAtCam.y);
			}
		}
		else //IF KEYBOARD (AND MOUSE)
		{
			Cursor.visible = false;
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = 10.0f;
			mousePos = Camera.main.ScreenToWorldPoint(mousePos);
			reticleRB.MovePosition(mousePos);
		}
	}

	public void Move () {
        //-------------------------------------------------
        //MenuScript.joystick = false; //REMOVE AFTER TESTING
        //-------------------------------------------------

        if (MenuScript.joystick)
        {
            //Debug.Log("Using controller: " + MenuScript.joystick);
        }
        else
        {
            //Debug.Log("Using controller: " + MenuScript.joystick);
        }
        Vector2 input;

		if (MenuScript.joystick) 
			input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		else 
			input = new Vector2(Input.GetAxis("HorizontalK"), Input.GetAxis("VerticalK"));

		playerRB.AddRelativeForce (input * (moveSpeed * moveSpeed), ForceMode2D.Impulse);
		moving = true;
	
		if (MenuScript.joystick)  //IF JOYSTICK PRESENT
		{
			//Stopping the player
			if (Input.GetAxis ("Horizontal") < 0.2f && Input.GetAxis ("Vertical") < 0.2f) {
				if (playerRB.velocity.magnitude < deadPoint) {
					playerRB.velocity -= playerRB.velocity * 2 * Time.deltaTime;
					animator.SetBool ("isMoving", false);
					moving = false;
				} else if (playerRB.velocity.magnitude < 0.05f) {
					playerRB.velocity = Vector3.zero;
					animator.SetBool ("isMoving", false);
					moving = false;
				} 
			}

			//Catching all values to set animation
			if (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0)
			{
				animator.SetBool ("isMoving", true);
				//Debug.Log("Player is moving");
				moving = true;
			} 
			else if (playerRB.velocity.magnitude < deadPoint*2) {
				animator.SetBool ("isMoving", false);
				moving = false;
			}
			else {
				animator.SetBool ("isMoving", false);
				moving = false;
			}

			//Flipping the player if he is facing left
			if (Input.GetAxis ("Horizontal") > 0 && flip && !isRight)
				{
				//Debug.Log ("Turned right");
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				flip = false;
				isRight = true;

			}
			//Flipping the player if he is facing right
			if (Input.GetAxis ("Horizontal") < 0 && isRight)
			{
				//Debug.Log ("Turned left");
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				isRight = false;
				flip = true;
			}
			if ((Mathf.Abs(Input.GetAxis ("Horizontal")) > 0.01f || Mathf.Abs(Input.GetAxis ("Vertical")) > 0.01f) && soundTimer < 0.001f) {
				//Debug.Log ("Hori: " + Input.GetAxis ("Horizontal") + ", Verti: " + Input.GetAxis ("Vertical"));
				soundManager.PlaySoundArray (walking, Random.Range(0.8f,1f));
				soundTimer = 0.15f;

			} else if ((Mathf.Abs(Input.GetAxis ("Horizontal")) < 0.01f || Mathf.Abs(Input.GetAxis ("Vertical")) < 0.01f)) {
				//Debug.Log ("Stopped walking");
			}


		}
		else //Keyboard detection
		{
			//Enabling the moving animation and sound if the player is moving
			if (input.x == 1 || input.x == -1 || input.y == 1 || input.y == -1) {
				animator.SetBool("isMoving", true);
				if (soundTimer < 0.001f) {
					soundManager.PlaySoundArray (walking, Random.Range(0.8f,1f));
					soundTimer = 0.15f;
				}
			}
			else //Disabling the moving animation and sound if the player is moving
			{
				animator.SetBool("isMoving", false);
			}

			//Flipping the player if he is facing left
			if (input.x == 1 && flip && !isRight) {
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				flip = false;
				isRight = true;
			}

			//Flipping the player if he is facing right
			if (input.x == -1 && isRight) {
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				isRight = false;
				flip = true;
			}
		}
			
	}

	void Stun () {
		//Debug.Log ("Player stun");
		animator.SetBool ("isStun", true);


		//TODO: Lock player input somehow
		//TODO: wait for a while
		//animator.SetBool ("isStun", false);
	}

	public void setStun () {
		stunned = true;
	}

	void ResizeCollider () {
		SpriteRenderer spr = GetComponent<SpriteRenderer>();
		Vector3 v = spr.bounds.size;
		BoxCollider2D b = GetComponent<BoxCollider2D>() as BoxCollider2D;
		b.size = v;
	}
}
