﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public float speed = 2f;
	public float sprintSpeed = 0.25f;
	public float detectionRadius = 3f;
	public float distance = 1f;
	public float margin = 1f;
	public float changeDirectionLength = 2f;
	public AudioClip headbuttSound;
	public GameObject powPart;

	private Transform target;
	private float distanceToTarget;
	private Vector2 direction;
	private Rigidbody2D rb2d;
	private Animator animator;
	private Vector2 saveOffset = Vector2.zero;
	private bool flip = true;
	private bool isRight = false;
	private SoundSpawner soundManager;
	public float cooldownCounter = 3f;
	private float cooldownCounterAttack = 1.5f;
	private bool stunned = false;
	private bool canAttack = true;
	private bool isAttacking = false;
	public int hitToDie = 3;
	private int hits = 0;
	private float originalSpeed;
	private Camera2DFollow camControl;


	// Use this for initialization
	void Start () {
		camControl = Camera.main.GetComponent<Camera2DFollow>();
		animator = GetComponent<Animator> ();
		soundManager = GetComponent<SoundSpawner> ();
		rb2d = GetComponent<Rigidbody2D> ();
		soundManager = GetComponent<SoundSpawner> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		Flip ();
		originalSpeed = speed;
	}
	
	// Update is called once per frame
	void Update () {
		if (hits >= hitToDie)
			Destroy (gameObject);
		//Flip the enemy
		Flip ();
		//Move towards player
		//MoveTowardsPlayer ();
		if (!stunned) {
			MoveTowardsPlayer ();
//			//Debug.Log (target.name);
//			Debug.Log("enemy target pos: " + target.position);
//			//Debug.Log ("yea counter: " + cooldownCounter);
		} 
		if (stunned) {
			Stun ();
			cooldownCounter -= Time.deltaTime;
			if (cooldownCounter <= 0) {
				animator.SetBool("isHit", false);
				cooldownCounter = 3f;
				stunned = false;
				hits++;
//				//soundManager.StopAllSound ();
//				//soundManager.setVolume (1);
			}
		}

		if (isAttacking) {
			Attack ();
			rb2d.velocity = Vector3.zero;
			cooldownCounterAttack -= Time.deltaTime;
			if (cooldownCounterAttack > 0) {
				canAttack = false;
			}
			else {
				cooldownCounterAttack = 1f;
				isAttacking = false;
				canAttack = true;
			}
		}
	}


	void MoveTowardsPlayer () {
		Vector2 targetPos = target.position;

		//Sprint within radius
		distanceToTarget = Vector2.Distance (transform.position, target.position);
		Color detectionCircle = Color.red;
		detectionCircle.a = 0.25f;
		//DebugDraw.DrawSphere(transform.position, detectionRadius, detectionCircle);
		if (distanceToTarget < detectionRadius) {
			speed = sprintSpeed;
			//Debug.Log ("Gotta go fast");
		}
		else {
			speed = originalSpeed;
		}

		
		direction = (target.position - transform.position);
		direction = direction.normalized;
		Vector2 position2D = new Vector2(transform.position.x, transform.position.y);

		RaycastHit2D hit = Physics2D.Raycast(position2D + saveOffset, direction, distance);
	
		if (hit.collider != null) {
			Debug.DrawLine(position2D, position2D + (direction*distance), Color.red);
		
			if (hit.collider.gameObject.tag == "Obstacle" || hit.collider.gameObject.tag == "Enemy") {
				Vector3 direction3D = new Vector3(direction.x, direction.y, 0f);
				Vector3 obstacleRight = Vector3.Cross(direction3D, Vector3.forward);
				Vector3 obstacleLeft = Vector3.Cross(direction3D, -Vector3.forward);
				Debug.DrawRay(hit.point, obstacleRight, Color.red, 1f);
				Debug.DrawRay(hit.point, obstacleLeft, Color.blue, 1f);

				Vector2 obstacleRight2D = hit.point + new Vector2(obstacleRight.x, obstacleRight.y) * changeDirectionLength;
				Vector2 obstacleLeft2D = hit.point + new Vector2(obstacleLeft.x, obstacleLeft.y) * changeDirectionLength;

				float obstacleRight2TargetDist = Vector2.Distance(obstacleRight2D, target.position);
				float obstacleLeft2TargetDist = Vector2.Distance(obstacleRight2D, target.position);

				if (obstacleRight2TargetDist < obstacleLeft2TargetDist) {
					targetPos = obstacleRight2D;
					saveOffset = obstacleLeft;
				} else {
					targetPos = obstacleLeft2D;
					saveOffset = obstacleRight;
				}
			} else {
				saveOffset = Vector2.zero;
			}
		}
		transform.position = Vector2.MoveTowards (transform.position, targetPos, speed * Time.deltaTime);
 	}



	void OnCollisionEnter2D (Collision2D col) {

		if (col.gameObject.tag == "Player" && col != null && !stunned) {
			col.gameObject.GetComponent<PlayerController> ().setStun ();
			isAttacking = true;
			//Debug.Log ("isAttacking status: " + isAttacking);
		}
	}

	void Stun () {
		animator.SetBool ("isHit", true);
	}

	public void setStun () {
		stunned = true;
		canAttack = false;
	}

	void Attack() {
		if (canAttack) {
			animator.SetTrigger("isAttacking");
			//soundManager.PlaySingleSoundOnce (headbuttSound, 0.8f);
			camControl.ShakeCamera(0.2f, 0.5f);
			Vector3 playerPos = GameObject.FindWithTag("Player").transform.position;
			ParticleSpawner particleSpawner = GameObject.Find("Particle Spawner").GetComponent<ParticleSpawner>();
			Vector3 middlepoint = (transform.position + playerPos)/2;
			middlepoint.z = -2f;
			particleSpawner.ParticleSpawning(middlepoint, powPart);
		}
	}



	void Flip () {
		Vector3 relativePoint = transform.InverseTransformPoint (target.position);
		if (relativePoint.x < 0.0 && flip && !isRight) {
			//Debug.Log ("player is to left");
			Vector3 theScale = transform.localScale;
			theScale.x = theScale.x * 1;
			transform.localScale = theScale;
			flip = false;
			isRight = true;
		}
		if (relativePoint.x > 0.0 && isRight) {
			//Debug.Log ("player is to right");
			Vector3 theScale = transform.localScale;
			theScale.x = theScale.x * (-1);
			transform.localScale = theScale;
			isRight = false;
			flip = true;
		}
	}

	/*private IEnumerator WaitForAnimation (Animation animation) {
		do {
			yield return null;
		} while (animation.isPlaying);
	}*/


}
