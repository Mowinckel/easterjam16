﻿using UnityEngine;
using UnityEngine.UI; // obsolete, but may become usefull.
using System.Collections;

public class IngameMenuScript : MonoBehaviour
{
    public Canvas inGameMenuCanvas; // This is the Main Canvas.
    public Canvas controlsIMGCanvas; // This is the Controls Canvas.
    public Canvas confirmActionCanvas; // This is the ConfirmExit Canvas.
    private bool menuVisibility = false; // Maybe an obsolete variable.

    void Start()
    {
        inGameMenuCanvas.enabled = false;
        controlsIMGCanvas.enabled = false;
        confirmActionCanvas.enabled = false;
    } // Use this for initialization - Hides the InGameMenu Completely OnLoadScene.


    void Update()
    {

        StartCoroutine("ToggleMenu");  //Toggles Ingame Menu with the controller's "Play"-button, and if open closes it with the controller's "(A)"-Button
        IfChain(); //Controls Controller Input logic

    }// Update is called once per frame - Controls the InGameMenu's components and interaction.

    public void MasterFunction(int FID)
    {
        if(FID == 1) // Attempts to quit to the main menu, but will ask for confirmation first
        {
            confirmActionCanvas.enabled = true;
            inGameMenuCanvas.enabled = false;
        }
        if(FID == 2) // If the Controller's "(X)"-button is pressed, it shows the controls for game.
        {
            inGameMenuCanvas.enabled = true;
            controlsIMGCanvas.enabled = true;
        }
        if(FID == 3) // Unpauses the Game
        {
            inGameMenuCanvas.enabled = false;
            PauseGame(false);
        }
        if(FID == 4) // If the Controller layout is shown, and the Controller's "(B)"-button is pressed it returns to the Ingame Menu GUI
        {
            inGameMenuCanvas.enabled = true;
            controlsIMGCanvas.enabled = false;
        }
        if(FID == 5) // Cancels ExitGame
        {
            confirmActionCanvas.enabled = false;
            inGameMenuCanvas.enabled = true;
        }
        if(FID == 6) // Exits Game
        {
            ChangeScene("Menu");
        }
    } //Executes the intended action when called, based on the Argument. This Method is called by the IfChain Method, and the UI-Buttons using it.
    public void ChangeScene(string sceneName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
    } // This IS Clickable by Default, if applied to a button (is Obsolete unless more than 1 scene should be loadable from the InGameMenu) This method is called by the MasterFunction method.

    private void PauseGame(bool state)
    {
        if (state)
        {
            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    } // Pauses the game, by setting timescale to either 0.0f or unpause with 1.0f , this method is called by the ToogleMenu Method.
    private void IfChain()
    {
        if (Input.GetButtonDown("Fire2") && inGameMenuCanvas.enabled && !controlsIMGCanvas.enabled)
        {
            MasterFunction(1);
        } // Opens the ActionConfirmCanvas
        else if (Input.GetButtonDown("Fire3") && inGameMenuCanvas.enabled && !controlsIMGCanvas.enabled && !confirmActionCanvas.enabled)
        {
            MasterFunction(2);
        } // Opens the ControlsIMGCanvas
        
        else if (Input.GetButtonDown("Fire2") && controlsIMGCanvas.enabled)
        {
            MasterFunction(4);
        } // Hides the ControlsIMGCanvas
        else if (Input.GetButtonDown("Fire2") && confirmActionCanvas.enabled)
        {
            MasterFunction(5);
        } // Cancels the Exit Game 
        else if (Input.GetButtonDown("CButton1") && confirmActionCanvas.enabled)
        {
            MasterFunction(6);
        } // Confirms the ExitGame
    }//Controls Controller Input logic, this method is called by the Update Method.
    IEnumerator ToggleMenu()
    {
        if (Input.GetButtonDown("StartCB") && !inGameMenuCanvas.enabled && !confirmActionCanvas.enabled || Input.GetKeyDown(KeyCode.P) && !inGameMenuCanvas.enabled && !confirmActionCanvas.enabled)
        {
            inGameMenuCanvas.enabled = true;
            PauseGame(true);
            yield return new WaitForSeconds(1f);
        }
        if (Input.GetButtonDown("CButton1") && inGameMenuCanvas.enabled && !confirmActionCanvas.enabled)
        {
            MasterFunction(3);
            yield return new WaitForSeconds(1f);
        }
        if (Input.GetButtonDown("StartCB") && inGameMenuCanvas.enabled && !confirmActionCanvas.enabled)
        {
            MasterFunction(3);
            yield return new WaitForSeconds(1f);
        }
        if (Input.GetKeyDown(KeyCode.P) && inGameMenuCanvas.enabled && !confirmActionCanvas.enabled)
        {
            MasterFunction(3);
            yield return new WaitForSeconds(1f);
        }
    }//Toggles Ingame Menu with the controller's "Play"-button, and if open closes it with the controller's "(A)"-Button , this method is started by a coroutine in the Update method.
}
