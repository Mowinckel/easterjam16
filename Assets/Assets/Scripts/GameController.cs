﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	//public CameraController camc;
	//public GameObject[] camObjs;
	//public int numberOfEnemies = 4;

	//public GameObject player;
	public GameObject enemyPrefab;

	public AudioSource[] audSrc;
	public AudioClip introMusic;
	public Transform[] spawnPoints;
	public Text textTables;
	public Text textTime;
	AudioSource main;
	AudioSource intro;
	AudioSource ambience;


	private GameObject playerObj;

//	private bool spawned = false;
//	private bool introDone = false;
	private float startTime;
	public float spawnTime = 3f;
	public int initialEnemies = 3;
	private float spawnCounter;
	public GameObject[] obstacles;
	private int obstacleCount;
	public float timeUntilLose = 30f;
	public bool spawning = true;




	// Use this for initialization
	void Start () {
		if (spawning) {
			for (int i = 0; i < initialEnemies; i++) {
				Spawn ();
			}
		}
		spawnCounter = spawnTime;
		obstacleCount = obstacles.Length;
		//textTables = gameObject.GetComponent<Text> ();
		//textTime = gameObject.GetComponent<Text> ();

	}

	// Update is called once per frame
	void Update () {
		timeUntilLose -= Time.deltaTime;
		//Debug.Log("Time left: " + timeUntilLose);
		if (spawning) {
			spawnCounter -= Time.deltaTime;
			if (spawnCounter <= 0) {
				Spawn ();
				Debug.Log ("Enemy spawned");
				spawnCounter = spawnTime;
			}
		}
		textTime.text = "" + Mathf.FloorToInt(timeUntilLose);
		textTables.text = "" + obstacleCount;
		Winning ();
		Losing ();
	}

	void Spawn () {
		int randomSpawn = Random.Range(0,spawnPoints.Length - 1);
		GameObject enemyObj = Instantiate (enemyPrefab, spawnPoints[randomSpawn].transform.position, Quaternion.identity) as GameObject;
	}

	public void subtractFromObstacles (bool hit) {
		if (hit) {
			obstacleCount--;
			Debug.Log ("Obstacles left: " + obstacleCount);
		}
		hit = false;
	}

	public void Winning () {
		if (obstacleCount <= 0) {
			Debug.Log ("You won");
            UnityEngine.SceneManagement.SceneManager.LoadScene("WinScreen");

        }
	}

	public void Losing () {
		if (timeUntilLose <= 0) {
			Debug.Log ("You lost");
            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");

        }
	}
		
}
