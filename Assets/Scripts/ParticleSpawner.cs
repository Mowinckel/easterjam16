﻿using UnityEngine;
using System.Collections;

public class ParticleSpawner : MonoBehaviour {

	private GameObject instancedObject;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ParticleSpawning (Vector3 position, GameObject particlePrefab) {
		//Instantiate object at origin's position
		instancedObject = Instantiate(particlePrefab, position, Quaternion.identity) as GameObject;

		//Play particle
		ParticleSystem partSys = instancedObject.GetComponent<ParticleSystem>();
		partSys.Play();

		//Match particle duration to sound duration
		AudioSource aSrc = instancedObject.GetComponent<AudioSource>();
		float aClipDuration = aSrc.clip.length;
		partSys.startLifetime = aClipDuration;

		//Destroy particle
		Destroy(instancedObject, aClipDuration);
	}
}
