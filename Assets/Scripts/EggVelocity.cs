﻿using UnityEngine;
using System.Collections;

public class EggVelocity : MonoBehaviour {


	public int speedConstant = 3;
	public int rotateSpeed = 50;
	public AudioClip[] impactSounds;
	public Sprite[] sprites;
	public GameObject explosion;

	private SoundSpawner soundManager;
	private GameObject target;
	private Vector3 targetPos;
	private Rigidbody2D rb2D;
	private SpriteRenderer spriteOnObject;
	private GameObject gameController;

	// Use this for initialization
	void Start () {
		gameController = GameObject.Find ("GameController");
		soundManager = GetComponent<SoundSpawner> ();
		target = GameObject.FindGameObjectWithTag ("Target");
		targetPos = target.transform.position;
		rb2D = gameObject.GetComponent<Rigidbody2D>();
		spriteOnObject = gameObject.GetComponent<SpriteRenderer> ();
		int randomSprite = Random.Range (0, sprites.Length);
		Sprite sprite = sprites[randomSprite];
		spriteOnObject.sprite = sprite;
	}
	
	// Update is called once per frame
	void Update () {
		Shoot();
		Destroy (gameObject, 3.0f);
		//PlaySound (obstacleDestroyed, explosion);

	}

	void Shoot () {
		
		transform.position = Vector3.MoveTowards(transform.position, targetPos, speedConstant *Time.deltaTime);
		//transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 360), Time.deltaTime);
		rb2D.angularVelocity = rotateSpeed*Random.Range(0.9f, 2.0f);

		if (transform.position == targetPos) {
			Destroy(gameObject);
			//Debug.Log("Die");
		} 
	}

	void OnCollisionEnter2D (Collision2D col) {
		if (col.gameObject.tag == "Enemy") {
			col.gameObject.GetComponent<EnemyController> ().setStun ();
			soundManager.PlaySoundArray(impactSounds, 1f);
			Destroy(gameObject);
		} else if (col.gameObject.tag == "Player") {
			Physics2D.IgnoreCollision (GetComponent<Collider2D> (), col.collider);
		} else if (col.gameObject.tag == "Obstacle") {
			gameController.GetComponent<GameController> ().subtractFromObstacles (true);
			ParticleSpawner particleSpawner = GameObject.Find("Particle Spawner").GetComponent<ParticleSpawner>();
			particleSpawner.ParticleSpawning(col.gameObject.transform.position, explosion);
			Destroy (col.gameObject);
			Destroy (gameObject);
		} else {
		}
	}


}
