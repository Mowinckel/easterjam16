﻿using UnityEngine;
using System.Collections;

public class SoundSpawner : MonoBehaviour {

	public float pitchRange = 0.2f;
	private float originalPitch;
	private int randomSound;
	private GameObject audioPlayer;
	private float audioLength;

	// Use this for initialization
	void Start () {
		audioPlayer = GameObject.FindWithTag("AudioPlayer");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlaySingleSoundOnce (AudioClip clip, float volume) {
		AudioSource audSrc = audioPlayer.AddComponent<AudioSource>();
		if (!audSrc.isPlaying) {
			audSrc.volume = volume;
			audSrc.clip = clip;
			audSrc.Play();
			audioLength = clip.length;
			Destroy(audSrc,audioLength);
		}
	}

	public void PlaySoundArray (AudioClip[] clipArray, float volume) {
		AudioSource audSrc = audioPlayer.AddComponent<AudioSource>();
		if (!audSrc.isPlaying) {
			audSrc.volume = volume;
			int randomSound = Random.Range(0, clipArray.Length-1);
			audSrc.clip = clipArray[randomSound];
			audSrc.Play();
			audioLength = clipArray[randomSound].length;
			Debug.Log("Clip: " + clipArray[randomSound].name);
			Destroy(audSrc,audioLength*2);
		}
	}


	public void PlaySingleSoundLoop (AudioClip clip, float volume) {
		AudioSource audSrc = audioPlayer.AddComponent<AudioSource>();
		if (!audSrc.isPlaying) {
			audSrc.volume = volume;
			audSrc.loop = true;
			audSrc.clip = clip;
			audSrc.Play();
			audioLength = clip.length;
			Destroy(audSrc,audioLength);
		}
	}

//	public void StopAllSound () {
//		audioSource.Stop ();
//	}
//
//	public void PauseAllSound () {
//		audioSource.Pause ();
//	}
//
//	public void PlayAllSound () {
//		audioSource.Play ();
//	}
//
//	public bool CheckPlaying () {
//		if (audioSource.isPlaying)
//			return true;
//		else
//			return false;
//	}
//
//	public void setVolume (float value) {
//		//Volume value between 0 - 1 f
//		audioSource.volume = value;
//	}
}
