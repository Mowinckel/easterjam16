﻿using UnityEngine;
using System.Collections;


public class ShootEggs : MonoBehaviour {

    public GameObject eggPrefab;
	public AudioClip[] throwingSounds;
	private SoundSpawner soundManager;
    private bool canShoot = true;
    public bool canShootPub = true;
    public float coolDowntime = 5.0f;
	// Use this for initialization
	void Start () {
		soundManager = GetComponent<SoundSpawner> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (MenuScript.joystick) {
			if (Input.GetAxis("PrimaryAttack") != 0) {
				if (canShoot) {
					StartCoroutine (ThrowEgg());
					//Debug.Log ("Egg!");
				}
					
			}
		}
		else {
			//Cursor.visible = false;
			if (Input.GetMouseButtonDown(0)) {
				if (canShoot) {
					StartCoroutine (ThrowEgg());
					//Debug.Log ("Egg!");
                    
				}
			}
		}

    }	

	IEnumerator ThrowEgg () {
		Instantiate (eggPrefab, transform.position, Quaternion.identity);
		soundManager.PlaySoundArray(throwingSounds,0.9f);
        canShoot = false;
        yield return new WaitForSeconds(coolDowntime);
        canShoot = true;
    }
    public void CDGUIMethod() // Used in Subclass CDGUI which is used for the cool down gui !!!DO NOT REMOVE!!!
    {
        if (MenuScript.joystick)
        {
            if (Input.GetAxis("PrimaryAttack") < -0.2)
                if (canShoot && canShootPub)
                    StartCoroutine(CoolDownMethod());
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
                if (canShoot && canShootPub)
                    StartCoroutine(CoolDownMethod());
        }
    }
IEnumerator CoolDownMethod() // Paralele coroutine used in CDGUIMethod() !!!DO NOT REMOVE!!!
    {
        canShootPub = false;      
        yield return new WaitForSeconds(coolDowntime);
        canShootPub = true;
    }
}